package com.velykyi.generic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Dog dog = new Dog("Sharik");
        Cat cat = new Cat("Murka");
        List<Dog> animals = new ArrayList<Dog>();
        animals.add(dog);
        //animals.add(cat);
        //logger1.info("Result of searching is " + Animal.find(animals, cat));
        logger1.info("Result of searching is " + Animal.find(animals, dog));
    }
}
