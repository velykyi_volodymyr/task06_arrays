package com.velykyi.generic;

import java.util.List;

public class Container<T> {
    T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }


}
