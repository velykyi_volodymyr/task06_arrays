package com.velykyi.generic;

import java.util.List;

public class Animal {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Animal(String name) {
        this.name = name;
    }

    public static <T extends Animal> boolean find(List<T> animals, T animal){
        for (Animal el : animals) {
            if (el.name.equals(animal.name)) {
                return true;
            }
        }
        return false;
    }
}
