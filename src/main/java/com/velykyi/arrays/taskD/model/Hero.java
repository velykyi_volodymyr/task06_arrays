package com.velykyi.arrays.taskD.model;

public class Hero implements Model {
    int heroForce;

    public Hero() {
        this.heroForce = 25;
    }

    public void addForce(int artifactForce) {
        this.heroForce += artifactForce;
    }

    public void subtractForce(int monsterForce) {
        this.heroForce -= monsterForce;
    }
}
