package com.velykyi.arrays.taskD.model;

public class Game {
    public Hero hero;
    public Room room;
    private int[] way = new int[10];

    public Game(Hero hero, Room room) {
        this.hero = hero;
        this.room = room;
    }

    private void nextDoor(int[] heroWay, int index) {
        for (int i = 0; i < 10; i++) {
            if (this.room.doors[i][0] == 0) {
                if ((this.hero.heroForce - this.room.doors[i][1]) < 0) {
                    break;
                } else {
                    heroWay[index] = i;
                    index++;
                    hero.subtractForce(this.room.doors[i][1]);
                    nextDoor(heroWay, index);
                }
            } else if (this.room.doors[i][1] == 1) {
                heroWay[index] = i;
                index++;
                hero.addForce(this.room.doors[i][1]);
            }
        }
    }

    public void aliveHero() {
        this.nextDoor(this.way, 0);
    }

    private boolean inArray(int[] array, int e) {
        boolean isInArray = false;
        for (int number : array) {
            if (number == e) {
                isInArray = true;
                break;
            }
        }
        return isInArray;
    }
}
