package com.velykyi.arrays.taskD.model;

public interface Model {
    void addForce(int artifactForce);
    void subtractForce(int monsterForce);
}
