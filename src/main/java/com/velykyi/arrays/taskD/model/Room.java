package com.velykyi.arrays.taskD.model;

import java.util.Random;

public class Room {
    int[][] doors;

    public Room() {
        this.doors = new int[10][2];
        for (int i = 0; i < 10; i++){
            this.doors[i][0] = new Random().nextInt(1);
            if (this.doors[i][0] == 0) {
                this.doors[i][1] = new Random().nextInt(96) + 5;
            } else if (this.doors[i][0] == 1) {
                this.doors[i][1] = new Random().nextInt(71) + 10;
            }
        }
    }
}
