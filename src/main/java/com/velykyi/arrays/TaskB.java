package com.velykyi.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskB {

    private static Logger logger1 = LogManager.getLogger(TaskB.class);

    public static int[] createUnique(int[] array1) {
        int size = array1.length;
        int[] array2 = new int[size];
        int count = 0;
        int j = 0;
        for (int i = 0; i < size; i++) {
            for (int e : array1) {
                if (array1[i] == e) {
                    count++;
                }
            }
            if (count <= 2) {
                array2[j] = array1[i];
                j++;
            }
            count = 0;
        }
        int[] resultArray = new int[j];
        System.arraycopy(array2, 0, resultArray, 0, j);
        logger1.info("TaskB: Array with elements which repeated less than two times was created!");
        return resultArray;
    }
}
