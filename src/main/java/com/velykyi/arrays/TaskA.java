package com.velykyi.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class TaskA {

    private static Logger logger1 = LogManager.getLogger(TaskA.class);

    public static int[] createAllElement(int[] array1, int[] array2) {
        int length3 = array1.length + array2.length;
        int[] array3 = new int[length3];
        int j = 0;
        for (int arr : array1) {
            array3[j] = arr;
            j++;
        }
        for (int arr : array2) {
            array3[j] = arr;
            j++;
        }
        logger1.info("TaskA: Array with all elements from two was created!");
        return array3;
    }

    public static int[] createUniqueElement(int[] array1, int[] array2) {
        int length3 = array1.length + array2.length;
        int[] array3 = new int[length3];
        int k = 0;
        boolean canAdd = true;
        for (int arr1 : array1) {
            for( int i = 0; i < k; i++){
                if (arr1 == array3[i]) {
                    canAdd = false;
                    break;
                } else {
                    canAdd = true;
                }
            }
            if (canAdd){
                array3[k] = arr1;
                k++;
            }
        }
        for (int arr2 : array2) {
            for( int i = 0; i < k; i++){
                if (arr2 == array3[i]) {
                    canAdd = false;
                    break;
                } else {
                    canAdd = true;
                }
            }
            if (canAdd){
                array3[k] = arr2;
                k++;
            }
        }
        int[] resultArray = new int[k];
        System.arraycopy(array3, 0, resultArray, 0, k);
        logger1.info("TaskA: Array with unique elements from two was created!");
        return resultArray;
    }
}
