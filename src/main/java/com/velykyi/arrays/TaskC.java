package com.velykyi.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskC {

    private static Logger logger1 = LogManager.getLogger(TaskC.class);


    public static int[] deleteDuplicate(int[] array1){
        int size = array1.length;
        int[] array2 = new int[size];
        int j = 0;
        for (int i = 0; i < size-1; i++){
            if (array1[i] != array1[i+1]) {
                array2[j] = array1[i];
                j++;
            }
        }
        array2[j] = array1[size-1];
        int[] resultArray = new int[j+1];
        System.arraycopy(array2, 0, resultArray, 0, j+1);
        logger1.info("TaskC: Array with unique elements was created!");
        return resultArray;
    }
}
