package com.velykyi.arrays;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Application {

    public static void main(String[] args) {
        int size = 10;
        int[] array1 = new int[size];
        int[] array2 = new int[size];

        for (int i = 0; i < 10; i++) {
            array1[i] = new Random().nextInt(size*3);
            array2[i] = new Random().nextInt(size*3);
        }
        System.out.println("Array1: ");
        for (int e : array1) {
            System.out.print(e + " ");
        }
        System.out.println("\nArray2: ");
        for (int e : array2) {
            System.out.print(e + " ");
        }
        System.out.println("\n\nTASK A:");
        int[] array3 = TaskA.createAllElement(array1, array2);
        System.out.println("All: ");
        for (int e : array3) {
            System.out.print(e + " ");
        }
        int[] array4 = TaskA.createUniqueElement(array1, array2);
        System.out.println("\nUnique: ");
        for (int e : array4) {
            System.out.print(e + " ");
        }
        System.out.println("\n\nTASK B:");
        int[] array5 = TaskB.createUnique(array3);
        System.out.println("Unique: ");
        for (int e : array5) {
            System.out.print(e + " ");
        }
        System.out.println("\n\nTASK C:");
        int[] array6 = {1, 1, 4, 1, 5, 7, 5, 6, 7, 7};
        int[] array7 = TaskC.deleteDuplicate(array6);
        System.out.println("Unique: ");
        for (int e : array7) {
            System.out.print(e + " ");
        }
    }
}
