package com.velykyi.collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Application {

    private static Logger logger1 = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        List<String> strList = new ArrayList<String>();
        Container container = new Container();
        for (int i = 0; i < 10; i++) {
            strList.add("element " + i);
            container.addElement("element " + i);
        }
        System.out.println(strList.get(3));
        System.out.println(container.getString(3));
        String[] strArray = container.getAllStrings();
        System.out.println("Get all strings from container: ");
        for (String el : strArray) {
            System.out.println(el);
        }
        System.out.println("Get some strings from container: ");
        strArray = container.getStrings(0, 5);
        for (String el : strArray) {
            System.out.println(el);
        }

        //-------------------------------------------------------------------------------------------------------------

        int size = 10;
        Country[] countryArray = new Country[size];
        for (int i = 0; i < size; i++) {
            countryArray[i] = new Country(GeneratorCountries.generateCountries());
        }
        List<Country> countryList = new ArrayList<Country>();
        for (int i = 0; i < size; i++) {
            countryList.add(new Country(GeneratorCountries.generateCountries()));
        }

        System.out.println("NOT SORTED ARRAY: ");
        for (Country el : countryArray) {
            System.out.println(el);
        }
        Arrays.sort(countryArray);
        logger1.info("Application: Array with country and capital was sorted.");
        System.out.println("\nSORTED ARRAY: ");
        for (Country el : countryArray) {
            System.out.println(el);
        }

        System.out.println("\n\nNOT SORTED LIST: ");
        for (Country el : countryList) {
            System.out.println(el);
        }
        Collections.sort(countryList);
        logger1.info("Application: ArrayList with country and capital was sorted.");
        System.out.println("\nSORTED LIST: ");
        for (Country el : countryList) {
            System.out.println(el);
        }

        //--------------------------------------------------------------------------------------------------------------

        CustomerDeque<Integer> integerDeque = new CustomerDeque<Integer>();
        for (int i = 0; i < 10; i++) {
            integerDeque.add(i);
        }
        System.out.println(integerDeque.getFirst());
        System.out.println(integerDeque.getLast());
        integerDeque.addFirst(11);
        integerDeque.addLast(12);
        integerDeque.remove(5);
        System.out.println(integerDeque.getFirst());
        System.out.println(integerDeque.getLast());

        while (!integerDeque.isEmpty()){
            System.out.println(integerDeque.remove());
        }
        //integerDeque.remove(null);
    }
}
