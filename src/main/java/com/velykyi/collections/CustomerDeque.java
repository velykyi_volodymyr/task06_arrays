package com.velykyi.collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class CustomerDeque<T> {
    private static Logger logger1 = LogManager.getLogger(Container.class);

    List<T> queue;

    public CustomerDeque() {
        this.queue = new ArrayList<T>();
    }

    public boolean add(T element) {
        queue.add(element);
        logger1.info("CustomerDeque: add() add new element to tail of deque.");
        return true;
    }

    public void addFirst(T element) {
        queue.add(0, element);
        logger1.info("CustomerDeque: addFirst() add new element to head of deque.");
    }

    public void addLast(T element) {
        queue.add(element);
        logger1.info("CustomerDeque: addLast() add new element to tail of deque.");
    }

    public boolean offer(T element) {
        queue.add(element);
        logger1.info("CustomerDeque: offer() add new element to tail of deque.");
        return true;
    }

    public T remove() {
        if (queue.isEmpty()) {
            logger1.info("CustomerDeque: remove() can not remove element because deque is EMPTY.");
            return null;
        }
        T element = queue.get(0);
        queue.remove(0);
        logger1.info("CustomerDeque: remove() remove element from head of deque.");
        return element;
    }

    public T poll() {
        if (queue.isEmpty()) {
            logger1.error("CustomerDeque: poll() throw NoSuchElementException, deque is EMPTY.");
            throw new NoSuchElementException("Deque is EMPTY!");
        }
        return queue.get(0);
    }

    public T element() {
        if (queue.isEmpty()) {
            return null;
        }
        return queue.get(0);
    }

    public T peek() {
        return queue.get(0);
    }

    public void push(T t) {
        addFirst(t);
    }

    public T pop() {
        return removeFirst();
    }

    public boolean remove(Object o) {
        if (o == null) {
            logger1.error("CustomerDeque: remove(Object o) throw NullPointerException, argument is NULL.");
            throw new NullPointerException("Argument is NULL!");
        }
        return contains(o);
    }

    public boolean containsAll(Collection<?> c) {
        return queue.containsAll(c);
    }

    public boolean addAll(Collection<? extends T> c) {
        logger1.error("CustomerDeque: addAll(Collection c) add all element from c.");
        return queue.addAll(c);
    }

    public boolean removeAll(Collection<?> c) {
        return queue.removeAll(c);
    }

    public boolean retainAll(Collection<?> c) {
        return queue.retainAll(c);
    }

    public void clear() {
        queue.clear();
    }

    public boolean contains(Object o) {
        return queue.contains(o);
    }

    public int size() {
        return queue.size();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public Iterator<T> iterator() {
        return queue.iterator();
    }

    public Object[] toArray() {
        return queue.toArray();
    }

    public <T1> T1[] toArray(T1[] a) {
        return queue.toArray(a);
    }

    public boolean offerFirst(T element) {
        queue.add(0, element);
        return true;
    }

    public boolean offerLast(T element) {
        queue.add(element);
        return true;
    }

    public T removeFirst() {
        return queue.remove(0);
    }

    public T removeLast() {
        return queue.remove(queue.size() - 1);
    }

    public T pollFirst() {
        T el = poll();
        removeFirst();
        return el;
    }

    public T pollLast() {
        if (queue.isEmpty()) {
            throw new NoSuchElementException("Deque is EMPTY!");
        }
        T el = queue.get(queue.size() - 1);
        removeLast();
        return el;
    }

    public T getFirst() {
        return queue.get(0);
    }

    public T getLast() {
        return queue.get(queue.size() - 1);
    }

    public T peekFirst() {
        if (queue.isEmpty()) {
            return null;
        }
        return getFirst();
    }

    public T peekLast() {
        if (queue.isEmpty()) {
            return null;
        }
        return getLast();
    }

    public boolean removeFirstOccurrence(Object o) {
        if (contains(o)) {
            for (Object el : queue) {
                if (el == o) {
                    remove(o);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean removeLastOccurrence(Object o) {
        if (contains(o)) {
            queue.remove(queue.lastIndexOf(o));
            return true;
        }
        return false;
    }


}
