package com.velykyi.collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class GeneratorCountries {

    private static Logger logger1 = LogManager.getLogger(Container.class);


    private static final String[][] COUNTRIES = {
            {"AFGHANISTAN",	"KABUL"},
            {"ALBANIA", "TIRANA"},
            {"ALGERIA",	"ALGIERS"},
            {"ANDORRA",	"ANDORRA LA VELLA"},
            {"ARMENIA",	"YEREVAN"},
            {"AUSTRALIA",	"CANBERRA"},
            {"AUSTRIA",	"VIENNA"},
            {"AZERBAIJAN",	"BAKU"},
            {"BAHAMAS", "THE NASSAU"},
            {"BAHRAIN",	"MANAMA"},
            {"BANGLADESH",	"DHAKA"},
            {"BARBADOS",	"BRIDGETOWN"},
            {"BELARUS",	"MINSK"},
            {"PHILIPPINES",	"MANILA"},
            {"POLAND",	"WARSAW"},
            {"PORTUGAL",	"LISBON"},
            {"REPUBLIC OF THE CONGO",	"BRAZZAVILLE"},
            {"ROMANIA",	"BUCHAREST"},
            {"UKRAINE",	"KIEV"},
            {"UNITED ARAB EMIRATES",	"ABU DHABI"},
            {"UNITED KINGDOM",	"LONDON"},
            {"UNITED STATES",	"WASHINGTON, D.C."}
    };

    public static String[] generateCountries() {
        logger1.info("GeneratorCountries: New random country was generated.");
        return COUNTRIES[new Random().nextInt(COUNTRIES.length)];
    }
}
