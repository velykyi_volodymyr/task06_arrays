package com.velykyi.collections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Container {

    private static Logger logger1 = LogManager.getLogger(Container.class);

    private String[] strArray;
    private int nextElementIndex;

    public Container() {
        this.strArray = new String[1];
        nextElementIndex = 0;
    }

    public void addElement(String str) {
        if (nextElementIndex >= strArray.length) {
            String[] newStrArray = new String[strArray.length + 1];
            for (int i = 0; i < strArray.length; i++) {
                newStrArray[i] = strArray[i];
            }
            newStrArray[nextElementIndex] = str;
            this.strArray = newStrArray;
            nextElementIndex++;
        } else {
              this.strArray[strArray.length - 1] = str;
              nextElementIndex++;
        }
        logger1.info("Container: Element was added.");
    }

    public String[] getAllStrings() {
        return this.strArray;
    }

    public String getString(int index) {
        return strArray[index];
    }

    public String[] getStrings(int start, int end) {
        return Arrays.copyOfRange(strArray, start, end);
    }
}
