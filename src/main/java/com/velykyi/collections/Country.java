package com.velykyi.collections;

public class Country implements Comparable<Country> {
    String country;
    String capital;

    public Country(String[] country) {
        this.country = country[0];
        this.capital = country[1];
    }

    @Override
    public String toString() {
        return country + " " + capital;
    }

    @Override
    public int compareTo(Country otherCountry) {
        return country.compareTo(otherCountry.country);
    }
}
