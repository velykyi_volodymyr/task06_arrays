package com.velykyi.queue;

public class Customer {
    String name;
    int priorityId;

    public Customer(String name, int priorityId) {
        this.name = name;
        this.priorityId = priorityId;
    }

    @Override
    public String toString() {
        return name + " " + priorityId;
    }
}
