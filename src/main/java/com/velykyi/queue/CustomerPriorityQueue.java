package com.velykyi.queue;

import com.velykyi.generic.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class CustomerPriorityQueue<T>{
     private List<Customer> itemList;
     private static Logger logger1 = LogManager.getLogger(CustomerPriorityQueue.class);

    public CustomerPriorityQueue() {
        this.itemList = new ArrayList<Customer>();
    }

    public <E extends Customer> void insert(E item){
        boolean isAdd = false;
        if (item != null) {
            for (Customer el : itemList) {
                if (el.priorityId < item.priorityId) {
                    itemList.add(itemList.indexOf(el), item);
                    logger1.info("Item is added: " + item);
                    isAdd = true;
                    break;
                }
            }
            if (!isAdd) {
                logger1.info("Item is added: " + item);
                itemList.add(item);
            }
        }
    }

    public Customer extractMin(){
        if (itemList.size() != 0) {
            Customer item = itemList.get(itemList.size()-1);
            logger1.info("Item is extracted: " + item);
            itemList.remove(item);
            return item;
        } else {
            logger1.info("List is empty.");
            return null;
        }
    }
}
