package com.velykyi.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Application {

    public static void main(String[] args) {
        CustomerPriorityQueue<Customer> customerList = new CustomerPriorityQueue<Customer>();
        for (int i = 0; i < 10; i++) {
            customerList.insert(new Customer("Customer", new Random().nextInt(100)));
        }
        for (int i = 0; i < 11; i++) {
            customerList.extractMin();
        }
    }
}
